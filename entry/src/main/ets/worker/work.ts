import worker from '@ohos.worker';
import testNapi from 'libentry.so'
import hilog from '@ohos.hilog';

const workerPort = worker.workerPort;

workerPort.onmessage = function (data) {
    hilog.info(0x0000, 'sub thread', 'Test NAPI start');
    testNapiLeak()
}

function testNapiLeak(){
    setInterval(()=>{
        testNapi.nativeCallArkTS((value)=>{return value * 2;});
    },17)
}